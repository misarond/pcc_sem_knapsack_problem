//
// Created by misarond on 5.1.23.
//

#ifndef PCC_SEM_KNAPSACK_PROBLEM_FLAGSHANDLER_H
#define PCC_SEM_KNAPSACK_PROBLEM_FLAGSHANDLER_H


#include <string>
#include <vector>

using namespace std;

class FlagsHandler {
public:
    FlagsHandler(int argc, char** argv);
    [[nodiscard]] bool print_help() const;
    [[nodiscard]] bool user_input() const;
    [[nodiscard]] int use_threads() const;
    [[nodiscard]] bool ret_indices() const;
    [[nodiscard]] string test_set() const;
    [[nodiscard]] string get_algo() const;



private:
    vector<string> flags;
    bool help = false;
    bool cmd_line = false;
    bool indices = false;
    int threads = 2;
    string algo = "dyn";
    string tests_type = "all";


    void handle_flags(int argc, char **argv);
};


#endif //PCC_SEM_KNAPSACK_PROBLEM_FLAGSHANDLER_H
