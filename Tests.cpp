//
// Created by misarond on 11.1.23.
//

#include "Tests.h"

#include <utility>

using namespace std;

template <typename TimePoint>
chrono::milliseconds to_ms(TimePoint tp) {
    return chrono::duration_cast<chrono::milliseconds>(tp);
}

void Tests::run_tests() {
    if (tests_type == "all") {
        basic_tests();
        large_tests();
        random_tests();
    } else if (tests_type == "basic") {
        basic_tests();
    } else if (tests_type == "large") {
        large_tests();
    } else if (tests_type == "random") {
        random_tests();
    }
}

void Tests::basic_tests() {
    KnapsackProblem ks;
    KnapsackProblem ks_1;
    KnapsackProblem ks_2;

    cout << "Basic tests" << endl;
    // Problem 1
    vector<int> v = {505, 352, 458, 220, 354, 414, 498, 545, 473, 543};
    vector<int> w = {23, 26, 20, 18, 32, 27, 29, 26, 30, 27};
    int W = 67;
    print_test_spec(v, w, W, 1);

    ks.load_data(v, w, W);
    ks_1.load_data(v, w, W);
    ks_2.load_data(v, w, W);

    cout << "[Dynamic]" << endl;
    ks.solve_problem_dynamic();
    ks.get_idxs();
    cout << endl;

    cout << "[Dynamic with 3 threads]" << endl;
    ks_1.solve_problem_dynamic_thread(3);
    ks_1.get_idxs();
    cout << endl;

    cout << "[Recursive]" << endl;
    ks_2.solve_problem_recursive();
    ks_2.get_idxs();
    cout << "_________________________________________" << endl;

    // Problem 2
    v = {5, 4, 3, 2};
    w = {4, 3, 2, 1};
    W = 6;
    print_test_spec(v, w, W, 2);

    ks.load_data(v, w, W);
    ks_1.load_data(v, w, W);
    ks_2.load_data(v, w, W);

    cout << "[Dynamic]" << endl;
    ks.solve_problem_dynamic();
    ks.get_idxs();
    cout << endl;

    cout << "[Dynamic with 3 threads]" << endl;
    ks_1.solve_problem_dynamic_thread(3);
    ks_1.get_idxs();
    cout << endl;

    cout << "[Recursive]" << endl;
    ks_2.solve_problem_recursive();
    ks_2.get_idxs();
    cout << "_________________________________________" << endl;

}

void Tests::print_test_spec(vector<int> &v, vector<int> &w, int W, int idx) {
    cout << "Problem "<< idx <<":" << endl;
    cout << "........................................" << endl;
    cout << "Weights of items: " << endl;
    for (auto el : w) {
        cout << el << " ";
    }
    cout << endl;

    cout << "Values of items: " << endl;
    for (auto el : v) {
        cout << el << " ";
    }
    cout << endl;

    cout << "Capacity of the knapsack:" << endl;
    cout << W << endl;
    cout << "........................................" << endl;

}

void Tests::large_tests() {
    cout << "Large tests" << endl;
    vector<vector<vector<long>>> times(3, vector<vector<long>>(3, vector<long>(5)));
    vector<int> n = {1000, 10000, 100000};
    vector<int> W = {1000, 10000, 100000};

    string str;
    int tst_idx = 1;

    int col = 0;
    int row = 0;
    for (auto n_i : n) {
        for (auto W_i : W) {
            KnapsackProblem ks_0, ks_1, ks_2, ks_3, ks_4;
            vector<int> v(n_i, 1);
            vector<int> w;
            for (int i = 0; i < n_i; ++i) {
                w.push_back(i);
            }

            ks_0.load_data(v, w, W_i);
            ks_1.load_data(v, w, W_i);
            ks_2.load_data(v, w, W_i);
            ks_3.load_data(v, w, W_i);
            ks_4.load_data(v, w, W_i);

            if (col == 2 && row == 2) {
                auto start = std::chrono::high_resolution_clock::now();
                ks_4.solve_problem_dynamic_thread_mem_opt(2);
                auto end = std::chrono::high_resolution_clock::now();
                times[col][row][4] = to_ms(end - start).count();
                continue;
            }

            auto start = std::chrono::high_resolution_clock::now();
            ks_0.solve_problem_dynamic(true);
            auto end = std::chrono::high_resolution_clock::now();
            times[col][row][0] = to_ms(end - start).count();

            start = std::chrono::high_resolution_clock::now();
            ks_1.solve_problem_dynamic_thread(2, true);
            end = std::chrono::high_resolution_clock::now();
            times[col][row][1] = to_ms(end - start).count();

            start = std::chrono::high_resolution_clock::now();
            ks_2.solve_problem_dynamic_thread(4, true);
            end = std::chrono::high_resolution_clock::now();
            times[col][row][2] = to_ms(end - start).count();

            if (n_i != 100000) {
                start = std::chrono::high_resolution_clock::now();
                ks_3.solve_problem_recursive(true);
                end = std::chrono::high_resolution_clock::now();
                times[col][row][3] = to_ms(end - start).count();
            }

            start = std::chrono::high_resolution_clock::now();
            ks_4.solve_problem_dynamic_thread_mem_opt(2);
            end = std::chrono::high_resolution_clock::now();
            times[col][row][4] = to_ms(end - start).count();

            ++row;
            str="Tests done " + to_string(tst_idx*4) + "/33";
            cout << str << endl;
            ++tst_idx;
        }
        row = 0;
        ++col;
    }
    print_times_matrix(times);
}

void Tests::print_times_matrix(vector<vector<vector<long>>> &m_t) {

    vector<string> n = {"10^3", "10^4", "10^5"};
    vector<string> W = {"10^3", "10^4", "10^5"};
    int c_i = 0;
    int r_i = 0;
    for (auto &col : m_t) {
        cout << "Number of items: " << n[r_i] <<  "\t (time in [ms])"<< endl;
        cout << "Capacity |          Dynamic | Dyn. (2 threads) | Dyn. (4 threads) |        Recursive |  Dyn. (2 t + mem)" << endl;
        for (auto &row : col) {
            cout << W[c_i] << "    ";
            for (auto el : row) {
                cout << " |";
                if (el == 0) {
                    cout << "              NaN";
                } else {
                    for (int k = 0; k < 16 - log10(el); ++k) {
                        cout << " ";
                    }
                    cout << el;
                }
            }
            cout << "\n";
            c_i++;
        }
        cout << "\n";
        c_i = 0;
        r_i++;
    }
    cout << "________________________________________________________________________________________________________" << endl;
}

void Tests::print_times_matrix(vector<vector<long>> &m_t, vector<vector<int>> &m_r) {
    int r_i = 0;
    int c_i = 0;
    vector<long> avg(4);
    cout << "Time in [ms] (result)"<< endl;
    cout << "          |          Dynamic | Dyn. (2 threads) | Dyn. (4 threads) |        Recursive " << endl;
    for (auto &row : m_t) {
        cout << "Problem " << to_string(r_i + 1);
        for (auto el : row) {
            cout << " |";
            for (int k = 0; k < 12 - int(log10(el)) - int(log10(m_r[r_i][c_i])); ++k) {
                cout << " ";
            }
            cout << el << " (" << m_r[r_i][c_i] << ")";
            avg[c_i] += el / long(5);
            c_i++;
        }
        c_i = 0;
        cout << "\n";
        r_i++;
    }
    cout << "Mean [ms]";
    for (auto avg_i : avg) {
        cout << " |";
        for (int k = 0; k < 16 - int(log10(avg_i)); ++k) {
            cout << " ";
        }
        cout << avg_i;
    }
    cout << "\n";
    cout << "_______________________________________________________________________________________________" << endl;
}

void Tests::random_tests() {
    cout << "Random tests" << endl;
    cout << "Size of the problems: n = 10^4  capacity = ~10^4" << endl;
    vector<vector<long>> times(5, vector<long>(4));
    vector<vector<int>> res(5, vector<int>(4));
    vector<int> v;
    vector<int> w;

    random_device dev;
    mt19937 rng(dev());
    int W;
    for (int i = 0; i < 5; ++i) {
        KnapsackProblem ks_0, ks_1, ks_2, ks_3;
        uniform_int_distribution<mt19937::result_type> dist_W(9500,10500);
        uniform_int_distribution<mt19937::result_type> dist_w(50,100);
        uniform_int_distribution<mt19937::result_type> dist_v(100,200);
        W = dist_W(rng);
        for (int j = 0; j < 10000; ++j) {
            v.push_back(dist_v(rng));
            w.push_back(dist_w(rng));
        }
        ks_0.load_data(v, w, W);
        ks_1.load_data(v, w, W);
        ks_2.load_data(v, w, W);
        ks_3.load_data(v, w, W);

        auto start = std::chrono::high_resolution_clock::now();
        res[i][0] = ks_0.solve_problem_dynamic(true);
        auto end = std::chrono::high_resolution_clock::now();
        times[i][0] = to_ms(end - start).count();

        start = std::chrono::high_resolution_clock::now();
        res[i][1] = ks_1.solve_problem_dynamic_thread(2, true);
        end = std::chrono::high_resolution_clock::now();
        times[i][1] = to_ms(end - start).count();

        start = std::chrono::high_resolution_clock::now();
        res[i][2] = ks_2.solve_problem_dynamic_thread(4, true);
        end = std::chrono::high_resolution_clock::now();
        times[i][2] = to_ms(end - start).count();

        start = std::chrono::high_resolution_clock::now();
        res[i][3] = ks_3.solve_problem_recursive(true);
        end = std::chrono::high_resolution_clock::now();
        times[i][3] = to_ms(end - start).count();

        cout << "Tests done " + to_string(i + 1) + "/5" << endl;
    }
    print_times_matrix(times, res);
}

Tests::Tests(string type) : tests_type(std::move(type)){}


