//
// Created by misarond on 5.1.23.
//

#include "knapsack.h"
#include <cmath>
#include <thread>

using namespace std;

void KnapsackProblem::load_data() {
    cout << "Please insert values of n items seperated by a space:" << endl;
    n = load_vector(&values);
    cout << "Please insert weights of n items seperated by a space:" << endl;
    int check_nbr_items = load_vector(&weights);
    if (n != check_nbr_items) {
        cout << "Number of items in values and weights must be the same!" << endl;
        exit(1);
    } else {
        cout << "Please insert capacity of the Knapsack:" << endl;
        cin >> capacity;
    }
}

void KnapsackProblem::load_data(vector<int> &v, vector<int> &w, int cap) {
    if (v.size() == w.size()) {
        n = int(v.size());
    } else {
        cout << "Number of items in values and weights must be the same!" << endl;
        exit(1);
    }
    values = v;
    values.insert(values.begin(), 0);
    weights = w;
    weights.insert(weights.begin(), 0);
    capacity = cap;
}

int KnapsackProblem::solve_problem_dynamic_thread(int nbr_t, bool large) {
    int ret = 0;
    if (!large) {
        m_full.clear();
    }
    m_full.resize(n + 1, vector<int>(capacity + 1));
    pthread_barrier_t barrier;
    pthread_barrier_init(&barrier, nullptr, nbr_t);
    auto work = [this, &barrier](int lb, int ub, int t_nbr) {
        for (int i = 1; i < n + 1; ++i) {
            for (int j = lb; j < ub + 1; ++j) {
                if (weights[i] > j) {
                    m_full[i][j] = m_full[i - 1][j];
                } else {
                    m_full[i][j] = max(m_full[i-1][j], m_full[i - 1][j - weights[i]] + values[i]);
                }
            }
            pthread_barrier_wait(&barrier);
        }
    };

    std::vector<std::thread> threads;
    int jump = capacity / nbr_t;
    for (int i = 0; i < nbr_t; i++) {
        if (i == nbr_t - 1) {
            threads.emplace_back(work, i*jump + 1, capacity, i);
        } else {
            threads.emplace_back(work, i*jump + 1, (i+1)*jump, i);
        }
    }

    for (auto& thread : threads) {
        thread.join();
    }

    ret = m_full[n][capacity];
    if (!large) {
        cout << "Maximum value we can get is: " << ret << endl;
    }
    if (large) {
        m_full.clear();
    }
    return ret;
}

int KnapsackProblem::solve_problem_dynamic(bool large) {
    int ret = 0;
    if (!large) {
        m_full.clear();
    }
    m_full.resize(n + 1, vector<int>(capacity + 1));
    for (int i = 1; i < n + 1; ++i) {
        for (int j = 1; j < capacity + 1; ++j) {
            if (weights[i] > j) {
                m_full[i][j] = m_full[i - 1][j];
            } else {
                m_full[i][j] = max(m_full[i-1][j], m_full[i - 1][j - weights[i]] + values[i]);
            }
        }
    }
    ret = m_full[n][capacity];
    if (!large) {
        cout << "Maximum value we can get is: " << ret << endl;
    }
    if (large) {
        m_full.clear();
    }
    return ret;
}


set<int> KnapsackProblem::get_indices_not_rec() {
    set<int> res;
    int j = capacity;
    for (int i = n; i > 0; --i) {
        if (m_full[i][j] > m_full[i-1][j]) {
            res.insert(i);
            j -= weights[i];
        }
    }
    return res;
}

int KnapsackProblem::load_vector(vector<int> *vec) {
    int nbr = 0;
    string line;
    getline(cin, line);
    stringstream v(line);
    int val;
    vec->push_back(0);
    while (v) {
        v >> val;
        vec->push_back(val);
        nbr++;
    }
    return nbr;
}

int KnapsackProblem::solve_problem_recursive(bool large) {
    int ret = 0;
    if (!large) {
        m_full.clear();
    }
    m_full.resize(n + 1, vector<int>(capacity + 1, -1));
    upd_val(n, capacity);
    ret = m_full[n][capacity];
    if (!large) {
        cout << "Maximum value we can get is: " << ret << endl;
    }
    if (large) {
        m_full.clear();
    }
    return ret;
}

void KnapsackProblem::upd_val(int i, int j) {
    if (i == 0 || j == 0) {
        m_full[i][j] = 0;
        return;
    }
    if (m_full[i-1][j] == -1) {
        upd_val(i-1, j);
    }
    if (weights[i] > j) {
        m_full[i][j] = m_full[i-1][j];
    } else {
        if (m_full[i-1][j-weights[i]] == -1) {
            upd_val(i-1, j-weights[i]);
        }
        m_full[i][j] = max(m_full[i-1][j], m_full[i-1][j-weights[i]] + values[i]);
    }
}

void KnapsackProblem::get_idxs() {
    cout << "Indices of used items are: ";
    indices = get_indices_not_rec();
    for (auto el : indices) {
        cout << el << " ";
    }
    cout << endl;
}

int KnapsackProblem::solve_problem_dynamic_thread_mem_opt(int nbr_t) {
    int ret = 0;
    m_opt.resize(2, vector<int>(capacity + 1));
    pthread_barrier_t barrier;
    pthread_barrier_init(&barrier, nullptr, nbr_t);
    auto work = [this, &barrier](int lb, int ub, int t_nbr) {
        for (int i = 1; i < n + 1; ++i) {
            for (int j = 1; j < capacity + 1; ++j) {
                if (weights[i] > j) {
                    m_opt[i % 2][j] = m_opt[(i - 1) % 2][j];
                } else {
                    m_opt[i % 2][j] = max(m_opt[(i - 1) % 2][j], m_opt[(i - 1) % 2][j - weights[i]] + values[i]);
                }
            }
            pthread_barrier_wait(&barrier);
        }
    };

    std::vector<std::thread> threads;
    int jump = capacity / nbr_t;
    for (int i = 0; i < nbr_t; i++) {
        if (i == nbr_t - 1) {
            threads.emplace_back(work, i*jump + 1, capacity, i);
        } else {
            threads.emplace_back(work, i*jump + 1, (i+1)*jump, i);
        }
    }

    for (auto& thread : threads) {
        thread.join();
    }
    ret = m_opt[n % 2][capacity];
    m_opt.clear();
    return ret;
}
