//
// Created by misarond on 11.1.23.
//

#ifndef KNAPSACK_TESTS_H
#define KNAPSACK_TESTS_H

#include <vector>
#include <chrono>
#include "knapsack.h"
#include <cmath>
#include <random>

class Tests {
public:
    Tests(string type);
    void run_tests();

private:
    static void basic_tests();
    static void large_tests();
    static void random_tests();
    static void print_test_spec(vector<int>& v, vector<int>& w, int W, int idx);
    static void print_times_matrix(vector<vector<vector<long>>>& m_t);

    string tests_type;

    static void print_times_matrix(vector<vector<long>> &m_t, vector<vector<int>> &m_r);
};


#endif //KNAPSACK_TESTS_H
