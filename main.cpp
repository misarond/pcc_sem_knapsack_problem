//
// Created by misarond on 5.1.23.
//
#include "knapsack.h"
#include "FlagsHandler.h"
#include "Tests.h"
#include <chrono>

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

int main(int argc, char **argv) {
    FlagsHandler fh(argc, argv);
    if (fh.print_help()) {
        return 0;
    }
    if (fh.user_input()) {
        KnapsackProblem ks;
        ks.load_data();
        string chosen_algo = fh.get_algo();
        if (chosen_algo == "dyn") {
            cout << "Algo used for solving is dynamic" << endl;
            ks.solve_problem_dynamic();
        } else if (chosen_algo == "dyn_t") {
            int nbr_t = fh.use_threads();
            cout << "Algo used for solving is dynamic with threads (" <<  nbr_t << ")" << endl;
            ks.solve_problem_dynamic_thread(nbr_t);
        } else if (chosen_algo == "rec") {
            cout << "Algo used for solving is recursive" << endl;
            ks.solve_problem_recursive();
        } else if (chosen_algo == "dyn_t_m") {
            int nbr_t = fh.use_threads();
            cout << "Maximum value we can get is: " << ks.solve_problem_dynamic_thread_mem_opt(nbr_t) << endl;
            cout << "Algo used for solving is dynamic with threads (" << nbr_t << ") and memory optimization" << endl;
        }
        if (fh.ret_indices() && chosen_algo != "dyn_t_m") {
            ks.get_idxs();
        }
    } else {
        auto t = Tests(fh.test_set());
        t.run_tests();
    }
    return 0;
}