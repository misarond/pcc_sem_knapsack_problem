//
// Created by misarond on 5.1.23.
//

#include <iostream>
#include "FlagsHandler.h"

FlagsHandler::FlagsHandler(int argc, char **argv) {
    for (int i = 0; i < argc; ++i) {
        flags.emplace_back(argv[i]);
    }
    handle_flags(argc, argv);
}

bool FlagsHandler::print_help() const {
    if (help) {
        string out = "   --------------------------------------------------------------------------------------------\n"
                     "   |                              0/1 Knapsack problem solver                                 |\n"
                     "   |                  See: https://en.wikipedia.org/wiki/Knapsack_problem                     |\n"
                     "   ____________________________________________________________________________________________\n"
                     "\n"
                     "usage: pcc_sem_knapsack_problem [--flags]\n"
                     "    Options:\n"
                     "      -c                  enables user to insert data via command line otherwise predefined tests are run\n"
                     "          -i                  shows also indices of items to use to get maximal value\n"
                     "          -t [int]            number of threads to use for problem solving, default=2\n"
                     "          --algo [string]     use algorithm for solving the problem, options are: 'dyn', 'dyn_t', 'dyn_t_m', 'rec', default='dyn'\n"
                     "                              (in case of use dyn_t_m indices can't be obtained)\n"
                     "      --tests [string]    choose demonstration test set , options are: 'all', 'basic', 'large', 'random', default='all'\n"
                     "\n"
                     "    Format for custom data input:\n"
                     "    Three lines have to be inserted:\n"
                     "      First contains values of n available items seperated by space.\n"
                     "      Second contains weights of n available items seperated by space.\n"
                     "      (Number of items in first and in second inserted array must be same.)\n"
                     "      Third contains maximal capacity of the knapsack.\n"
                     "\n"
                     "    Exit Status:\n"
                     "    Returns success unless invalid option is given.\n"
                     "\n";
        cout << out;
        return true;
    } else {
        return false;
    }
}

void FlagsHandler::handle_flags(int argc, char **argv) {
    string flag;
    for (int i = 1; i < argc; ++i) {
        flag = argv[i];
        flags.emplace_back(flag);
        if (flag == "--help") {
            help = true;
        }
        if (flag == "--algo") {
            string alg = argv[i+1];
            algo = std::move(alg);
        }
        if (flag == "--tests") {
            string tst_set = argv[i+1];
            tests_type = std::move(tst_set);
        }
        if (flag == "-t") {
            string nbr = argv[i+1];
            threads = stoi(nbr);
        } else if (flag[0] == '-') {
            int idx = 1;
            char next = flag[idx];
            if (next != '-' && next != '\n') {
                while (idx < flag.size()) {
                    if (next == 'c') {
                        cmd_line = true;
                    } else if (next == 'i') {
                        indices = true;
                    }
                    idx++;
                    next = flag[idx];
                }
            }
        }
    }
}

bool FlagsHandler::user_input() const {
    return cmd_line;
}

int FlagsHandler::use_threads() const {
    return threads;
}

bool FlagsHandler::ret_indices() const {
    return indices;
}

string FlagsHandler::test_set() const {
    return tests_type;
}

string FlagsHandler::get_algo() const {
    return algo;
}
