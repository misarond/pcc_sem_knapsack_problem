# 0/1 Knapsack problem

### Tag:

0.0.3

### SHA: 
6e83f8440a114355980b1c38e2819bab8d2b0a06

## Popis zadání

Úkolem bylo naprogramovat algoritmus řešící NP úplný 0/1 Knapsack problém.
Tato úloha je o nalezení maximální hodnoty, kterou lze získat pomocí výběru *k* předmětů.
Každý z celkového množství *n* předmětů má svou hodnotu a váhu, poslední parametr je potom maximální hmotnost *W*, 
která se vměstná do batohu (knapsack). Cílem je tedy nalézt kombinaci *k* předmětů, tak aby byla dodržena podmínka, 
že suma jejich hmostností je <= *W* a zároveň suma jejich hodnot je nejvyšší možná. Všechny tři proměnné (vektor hodnot,
 vektor vah a maximální hmotnost obsahují/jsou integer hodnoty). 

## Popis implementace

Celkově jsem vytvořil tři třídy:

- FlagsHandler, která se stará o načtení vstupních argumentů.
- Tests, která obsahuje tři sady testů:

    - Základní (basic), které lze vyřešit i ručně pro ověření správných výsledků.
    - Velké (large), kde se hodnoty *n* a *W* pohybují od 10^3 až do 10^5, nicméně vstupní data jsou velmi jednoduché,
      konkrétně vektor hodnot *v* je vektor samých 1 o velikosti  *n* a vektor vah *w* má na každé pozici váhu rovnou
      svému indexu a je také velikosti *n*.
    - Náhodné (random), kde je velikost *n* = 10^4, hodnoty vektoru *v* jsou náhodně vybrané z intervalu [100, 200] a 
      hodnoty vektoru *w* potom z intervalu [50, 100] a velikost *W* je náhodně zvolena z intervalu [9500, 10500].
- Knapsack, ve které jsou implementovány algoritmy pro řešení problému.

Řešení problému je v zásadě založeno na dvou algoritmech:

- Dynamické programování
   
    - V průběhu řešení se doplňuje matice o velikost *(n + 1) x (W + 1)*, první řádek i sloupec jsou vyplněny nulami. 
      Poté se postupně doplňují řádky, hodnota na indexech *(i, j)* je potom určena jako maximum z hodnot 
      *(i - 1, j)* a  *(i - 1, j - w[i]) + v[i]*, kde *w[i]* je hmotnost i-tého předmětu a *v[i]* jeho hodnota.
    - Pokud je hmotnost *w[i]* větší než *j* automaticky se doplňuje hodnota *(i - 1, j)*.
    - Výsledná maximální hodnota je potom hodnota v pravém spodním rohu tedy na indexech *(n, W)*.

- Rekurze
  
    - Funguje na podobném principu, pouze se nejdříve inicializuje celá matice hodnotami -1. Následně se začne od 
      pravého spodního rohu *(n, W)* a na jako jeho hodnota se opět vybere maximum z hodnot *(i - 1, j)* a *(i - 1, j - w[i]) + v[i]*, pokud jedna z těchto hodnot je rovna -1, tedy ještě nebyla
      vypočítána, algoritmus se zanoří o úroveň hlouběji a nejprve vypočítá hodnotu pro neznámé pole.
    - Tento krok se opakuje až do té doby než *i == 0* nebo *j == 0*, na těchto indexech je opět doplněna hodnota 0.

Rekurze oproti přístupu s dynamickým programováním nemusí exhaustivně projít celou matici a může se k výsledku dostat rychleji,
na druhou stranu některé prvky matice se v rekurzním volání opakují, což může způsobit i pomalejší výsledek.

Zrychlení lze docílit použitím vláken. Implementoval jsem vlákna v podstatě totožně jako první přístup s dynamickým 
programováním. Jediným rozdílem je, že jednotlivá vlákna si rozdělí sloupce ve vytvářené matici a nezávisle na sobě doplňují
hodnoty ve svých sloupcích. Jediná synchronizační akce je potřeba udělat, při postupu z i-tého řádku na (i + 1), kde je potřeba
počkat než všechny vlákna dokončí svou činnost, k tomu jsem využil bariéru.

Algoritmus je zároveň ale také náročný na paměť, jelikož se vytváří řádově *n x W* velká matice integerů. Paměťové optimalizace
jsem docílil pomocí zmenšení matice na dva řádky, protože k výpočtu i-tého řádku je potřeba znát pouze i - 1. Díky tomuto
přístupu, kde se tyto dva řádky iterativně přepisují lze docílit vyřešení větších problému, které by u předchozích přístupů
skončily na nedostatku paměti. Cenou za to je nemožnost zpětného určení předmětů, kterých je třeba pro maximální hodnotu
a také pomalejší rychlost.


## Popis funkčnosti a ovládání

Ovládání je popsáno v nápovědě, při spuštění programu s argumentem --help. V zásadě, lze zvolit mezi dvěma módy, kde jeden
spustí předefinované testy (pomocí argumetu --tests [all/basic/large/random] lze zvolit nějakou skupinu případně všechny)
a druhý, který se spustí pomocí argumentu -c, dovoluje uživateli řešit jím definovaný problém. Hodnoty proměnných *v*, *w*
a *W* se zadají přímo pomocí standardního vstupu. Ostatní argumenty slouží k navolení způsobu řešení problému zadaného
uživatelem.

## Výsledky

Z naměřených výsledků je zřejmé, že zrychlení pomocí vícevláknového řešení se projeví především u větších problémů, kde
"overhead" způsobený obsluhou vláken je zanedbatelný vzhledem k časové složitosti problému. Využití optimalizované 
paměťové verze, dovoluje vyřešit větší problémy za cenu celkového zpomalení.

Měření jsem provedl v Ubuntu 20.04 s procesorem Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz s 16 GB RAM.

### Velké (large) problémy

*n = 10^3*

Naměřené časy v [ms]

| W    | Dynamic | Dynamic (2 thr) | Dynamic (4 thr) | Recursive | Dynamic (2 thr, mem_opt) |
|------|---------|-----------------|-----------------|-----------|--------------------------|
| 10^3 | 2       | 5               | 6               | 4         | 3                        |
| 10^4 | 29      | 25              | 24              | 91        | 13                       |
| 10^5 | 301     | 231             | 208             | 1017      | 120                      |



*n = 10^4*

Naměřené časy v [ms]

| W    | Dynamic | Dynamic (2 thr)     | Dynamic (4 thr)    | Recursive | Dynamic (2 thr, mem_opt) |
|------|---------|---------------------|--------------------|-----------|--------------------------|
| 10^3 | 25      | 51                  | 50                 | 20        | 30                       |
| 10^4 | 262     | 236                 | 257                | 836       | 121                      |
| 10^5 | 2898    | 2349                | 2323               | 16103     | 1173                     |


*n = 10^5*

Naměřené časy v [ms]

| W    | Dynamic | Dynamic (2 thr) | Dynamic (4 thr) | Recursive  | Dynamic (2 thr, mem_opt) |
|------|---------|-----------------|-----------------|------------|--------------------------|
| 10^3 | 236     | 426             | 505             | NaN (653)  | 385                      |
| 10^4 | 2411    | 2245            | 2251            | NaN (8727) | 1018                     |
| 10^5 | NaN     | NaN             | NaN             | NaN        | 9945                     |

U rekurze jsou naměřené hodnoty v závorce v Debug módu, v Realease módu způsobovala SIGSEGV.

### Náhodné (random) problémy

Naměřené časy v [ms]

|           | Dynamic | Dynamic (2 thr) | Dynamic (4 thr) | Recursive |
|-----------|---------|-----------------|-----------------|-----------|
| Problem 1 | 289     | 249             | 273             | 1016      | 
| Problem 2 | 608     | 529             | 542             | 2080      | 
| Problem 3 | 881     | 774             | 853             | 3359      | 
| Problem 4 | 1151    | 988             | 1042            | 3926      | 
| Problem 5 | 1414    | 1246            | 1248            | 4786      |
| Mean [ms] | 866     | 754             | 789             | 3032      |
