//
// Created by misarond on 5.1.23.
//

#ifndef PCC_SEM_KNAPSACK_PROBLEM_KNAPSACK_H
#define PCC_SEM_KNAPSACK_PROBLEM_KNAPSACK_H
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <set>

using namespace std;

class KnapsackProblem {
public:
    void load_data();

    void load_data(vector<int>& v, vector<int>& w, int cap);

    int solve_problem_dynamic(bool large=false);

    int solve_problem_dynamic_thread(int nbr_t, bool large=false);

    int solve_problem_dynamic_thread_mem_opt(int nbr_t);

    int solve_problem_recursive(bool large=false);



    void get_idxs();
private:
    int n = 0;
    int capacity = 0;
    // Vectors values (v) and weights (w) contains dummy element 0 on index 0
    vector<int> values;
    vector<int> weights;
    vector<vector<int>> m_full;
    vector<vector<int>> m_opt;

    set<int> indices;
    // Loads vector v or w from cin
    static int load_vector(vector<int>* vec);
    // Helper function for recursive algorithm
    void upd_val(int i, int j);
    // Function which returns indices from full matrix m_full, starts from (n, W) element
    // and on each line looks whether i-th item was used or not
    set<int> get_indices_not_rec();
};


#endif //PCC_SEM_KNAPSACK_PROBLEM_KNAPSACK_H
